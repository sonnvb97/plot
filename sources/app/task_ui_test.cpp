#include "task_ui_test.h"
#include "ak.h"
#include "app.h"
#include "task_list.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ssd1306syp.h"

#include "xprintf.h"
#include "plot.h"
#include "plot_monochrome.h"
#include "timer.h"

plot_monochrome PlotObj(true);

Adafruit_ssd1306syp Render;

void task_ui_test(ak_msg_t* msg) {
	static Line_t *line1;
	switch (msg->sig) {
		case AC_UI_INITIAL:
		{
			Render.initialize();
			line1 = PlotObj.newLine();
			PlotObj.setDrawZone(64, 64);
			PlotObj.addPoint(line1, 0, 1);
			PlotObj.addPoint(line1, 1, 2);
			PlotObj.addPoint(line1, 1, 6);
			PlotObj.addPoint(line1, 1, 1);
			PlotObj.addPoint(line1, 1, 7);
			PlotObj.addPoint(line1, 1, 1);
			PlotObj.plotLine(line1, WHITE);
			PlotObj.Update();
			Render.clear();
			Render.drawBitmap(0, 0, (const uint8_t*)PlotObj.Bitmap,
							  PlotObj.getWidth(), PlotObj.getHeight(), WHITE);
			Render.update();
			task_post_pure_msg(AC_TASK_UI_ID, AC_UI_TEST);
			//timer_set(AC_TASK_UI_ID, AC_UI_TEST, 500, TIMER_PERIODIC);
			break;
		}
		case AC_UI_TEST:
		{
			int16_t temp = rand()%7;
			PlotObj.addPoint(line1, 1, temp);
			PlotObj.plotLine(line1, WHITE);
			PlotObj.Update();
			Render.clear();
			Render.drawBitmap(0, 0, (const uint8_t*)PlotObj.Bitmap,
							  PlotObj.getWidth(), PlotObj.getHeight(), WHITE);
			Render.update();
			task_post_pure_msg(AC_TASK_UI_ID, AC_UI_TEST);
			break;
		}
		default: break;
	}
}


