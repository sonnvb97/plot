#include "plot_ssd1306.h"
#include "cstdlib"

plot_ssd1306::plot_ssd1306(bool isTimeBased):Plot(isTimeBased)
{

}

int plot_ssd1306::initialize()
{
	Render.initialize();
	return 0;
}

int plot_ssd1306::DrawPixel(int16_t Column, int16_t Row, uint16_t Color)
{
	Render.drawPixel(Column, Row, Color);
	return 0;
}

int plot_ssd1306::Clear()
{
	Render.clear();
//	Render.drawLine(Origin.Column, Origin.Row, Origin.Column, 5, WHITE);
//	Render.drawLine(Origin.Column, Origin.Row, 120, Origin.Row, WHITE);
//	Render.setCursor(Origin.Column + 2, 5);
//	Render.write('y');
//	Render.setCursor(120, Origin.Row);
//	Render.write('x');
//	for(int i = 0; Origin.Row - Scale_y*2*i > 5+5; i++)
//	{
//		Render.drawLine(Origin.Column - 2, Origin.Row - Scale_y*2*i, Origin.Column, Origin.Row - Scale_y*2*i, WHITE);
//	}
//	for(int i = 0; Origin.Column + Scale_x*2*i < 120-5; i++)
//	{
//		Render.drawLine(Origin.Column + Scale_x*2*i, Origin.Row, Origin.Column + Scale_x*2*i, Origin.Row + 2, WHITE);
//	}
	drawAxis(WHITE);
	return 0;
}

int plot_ssd1306::Update()
{
	Clear();
	for(int i = 0; i < PLOT_NUM_LINE_PER_OBJ; i++)
	{
		if(LineList[i].isEnabled && LineList[i].isShow)
		{
			plotLine(&LineList[i], WHITE);
		}
	}
	Render.update();
	return 0;
}
