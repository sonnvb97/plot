#include "plot.h"

#define plot_abs(x) ((x)>0?(x):-(x))
#define plot_swap(a, b) { int16_t t = a; a = b; b = t; }

#define MINIMAL_DRAW_ZONE_SIZE	50



void Plot::CalculateDisplay()
{
	xDispMin = 0;
	yDispMin = 0;
	xDispMax = (DrawZone_Width-1)/Scale_x - 1;
	yDispMax = (DrawZone_Height-1)/Scale_y - 1;
}

Plot::Plot(bool isTimeBased)
{
	CurrentLineNumber = 0;
	for(int i = 0; i < PLOT_NUM_LINE_PER_OBJ; i++)
	{
		LineList[i].isEnabled = false;
		LineList[i].Color = 0xFFFF;
	}

	TimeBased = isTimeBased;

	DrawZone_Width = 128;
	DrawZone_Height = 64;

	Origin.Column = 2;
	Origin.Row = DrawZone_Height -1 - 2;

	Scale_x = 5;
	Scale_y = 5;
	drawScale_x = 1;
	drawScale_y = 1;

	CalculateDisplay();
}

int Plot::setScale(uint8_t scale)
{
	Scale_x = scale;
	Scale_y = scale;
	CalculateDisplay();
	return 0;
}

int Plot::setScale(uint8_t scale_x, uint8_t scale_y)
{
	Scale_x = scale_x;
	Scale_y = scale_y;
	CalculateDisplay();
	return 0;
}

int Plot::setAxisDrawScale(uint8_t scale)
{
	drawScale_x = scale;
	drawScale_y = scale;
	return 0;
}

int Plot::setAxisDrawScale(uint8_t scale_x, uint8_t scale_y)
{
	Scale_x = scale_x;
	Scale_y = scale_y;
	return 0;
}

int Plot::setDrawZone(int16_t width, int16_t height)
{
//	if(x1 > x2)
//	{
//		int16_t _temp = x1;
//		x1 = x2;
//		x2 = _temp;
//	}
//	if(y1 > y2)
//	{
//		int16_t _temp = y1;
//		y1 = y2;
//		y2 = _temp;
//	}

	if( (width < MINIMAL_DRAW_ZONE_SIZE) || (height < MINIMAL_DRAW_ZONE_SIZE))
		return -1;	// Draw Zone is too small
	DrawZone_Width = width;
	DrawZone_Height = height;
	Origin.Column = 2;
	Origin.Row = DrawZone_Height -1 - 2;
	CalculateDisplay();
	return 0;
}

int16_t Plot::getWidth()
{
	return DrawZone_Width;
}

int16_t Plot::getHeight()
{
	return DrawZone_Height;
}

Position_t Plot::CoordinateToPosition(Coordinate_t Point)
{
	Position_t RetValue;

	RetValue.Column = Origin.Column + Scale_x * Point.x;
	RetValue.Row = Origin.Row - Scale_y * Point.y;

	return RetValue;
}

int Plot::DrawPixel(int16_t Column, int16_t Row, uint16_t Color)
{
	(void)Column;
	(void)Row;
	(void)Color;

	return 0;
}

int Plot::DrawPosition(int16_t Column, int16_t Row, uint16_t Color)
{
	if(Row < 0)
		return -1;	// Out of Bounds - Up
	if(Row >= DrawZone_Height)
		return -2;	// Out of Bounds - Down
	if(Column < 0)
		return -3;	// Out of Bounds - Left
	if(Column >= DrawZone_Width)
		return -4;	// Out of Bounds - Right

	DrawPixel(Column, Row, Color);
	return 0;
}

int Plot::DrawPoint(Coordinate_t Point)
{
	Position_t _temp = CoordinateToPosition(Point);
	DrawPosition(_temp.Column, _temp.Row, 0);
	return 0;
}

int Plot::DrawLine(Coordinate_t Point1, Coordinate_t Point2, uint16_t Color)
{
	Position_t _temp1 = CoordinateToPosition(Point1);
	Position_t _temp2 = CoordinateToPosition(Point2);

	int16_t x1 = _temp1.Column, y1 = _temp1.Row;
	int16_t x2 = _temp2.Column, y2 = _temp2.Row;

	int16_t dx = plot_abs(x2 - x1);
	int16_t dy = plot_abs(y2 - y1);
	int16_t sx = (x1<x2)? 1: -1;
	int16_t sy = (y1<y2)? 1: -1;
	int16_t err = dx - dy;

	while(1)
	{
		DrawPosition(x1, y1, Color);
		if ( (x1 == x2) && (y1 == y2) )
		{
			break;
		}

		int16_t e2 = 2*err;
		if(e2 > -dy)
		{
			err -= dy;
			x1 += sx;
		}
		if(e2 < dx)
		{
			err += dx;
			y1 += sy;
		}
	}

	return 0;
}

int Plot::drawAxis(uint16_t Color)
{
	// Draw y-Axis
	// Axis line
	for(int16_t row = Origin.Row; row >= 0; row--)
	{
		DrawPosition(Origin.Column, row, Color);
	}
	// Arrow
	DrawPosition(Origin.Column - 1, 1, Color);
	DrawPosition(Origin.Column + 1, 1, Color);
	DrawPosition(Origin.Column - 2, 2, Color);
	DrawPosition(Origin.Column + 2, 2, Color);

	for(int16_t row = Origin.Row; row > drawScale_y*Scale_y; row-=drawScale_y*Scale_y)
	{
		DrawPosition(Origin.Column - 1, row, Color);
		DrawPosition(Origin.Column - 2, row, Color);
	}

	// Draw x-Axis
	// Axis line
	for(int16_t col = Origin.Column; col < DrawZone_Width; col++)
	{
		DrawPosition(col, Origin.Row, Color);
	}
	// Arrow
	DrawPosition(DrawZone_Width - 2, Origin.Row + 1, Color);
	DrawPosition(DrawZone_Width - 2, Origin.Row - 1, Color);
	DrawPosition(DrawZone_Width - 3, Origin.Row + 2, Color);
	DrawPosition(DrawZone_Width - 3, Origin.Row - 2, Color);

	for(int16_t col = Origin.Column; col < DrawZone_Width - drawScale_x*Scale_x; col+=drawScale_x*Scale_x)
	{
		DrawPosition(col, Origin.Row + 1, Color);
		DrawPosition(col, Origin.Row + 2, Color);
	}

	return 0;
}

Line_t* Plot::newLine(void)
{
	if(CurrentLineNumber >= PLOT_NUM_LINE_PER_OBJ)
	{
		return NULL;
	}
	int i;
	for(i = 0; i<PLOT_NUM_LINE_PER_OBJ; i++)
	{
		if(LineList[i].isEnabled == false)
		{
			break;
		}
	}
	LineList[i].isEnabled = true;
	LineList[i].isShow = false;
	LineList[i].isFull = false;
	LineList[i].Head = 0;
	LineList[i].Tail = 0;
	CurrentLineNumber++;
	return (LineList + i);
}

int Plot::deleteLine(Line_t* &line)
{
	if(!lineExisted(line))
	{
		return -1;	// Line not found in this scope
	}
	line->isEnabled = false;
	line = NULL;
	CurrentLineNumber--;
	return 0;
}

bool Plot::lineExisted(Line_t* line)
{
	int _temp = line - LineList;
	return((_temp >= 0) && (_temp < CurrentLineNumber) && line->isEnabled);
}

int Plot::addPoint(Line_t* line, int16_t x, int16_t y)
{
	if(!lineExisted(line))
	{
		return -1;	// Line not found in this scope
	}

	Coordinate_t *_pPoint = line->PointList;
	uint16_t	&_index = line->Tail;
	uint16_t	&_head = line->Head;

	if(!TimeBased)
	{
		_pPoint[_index].x = x;
		_pPoint[_index].y = y;
	}
	else if(_index == 0 && !line->isFull)
	{
		_pPoint[_index].x = x;
		_pPoint[_index].y = y;
	}
	else
	{
		uint16_t _preIndex = (_index)? _index - 1 : PLOT_NUM_POINT_PER_LINE - 1;
		_pPoint[_index].x = _pPoint[_preIndex].x + x;
		_pPoint[_index].y = y;

		if(_pPoint[_index].x > xDispMax)
		{
			int16_t dif = _pPoint[_index].x - xDispMax;
			for(uint16_t i = _head; ; i++)
			{
				if(i>= PLOT_NUM_POINT_PER_LINE)
					i -= PLOT_NUM_POINT_PER_LINE;
				if(i == _index)
					break;
				_pPoint[i].x -= dif;
				if(_pPoint[i].x < xDispMin)
				{
					_head++;
					if(_head>= PLOT_NUM_POINT_PER_LINE)
						_head -= PLOT_NUM_POINT_PER_LINE;
				}
			}
			_pPoint[_index].x -= dif;
		}

	}
	_index++;
	if(_index >= PLOT_NUM_POINT_PER_LINE)
	{
		_index = 0;
		line->isFull = true;
	}

	return 0;	// OK
}

int Plot::plotLine(Line_t* line, uint16_t Color)
{
	if(!lineExisted(line))
	{
		return -1;	// Line not found in this plot
	}
	if(line->Tail == 0 && !line->isFull)
	{
		return -2;	// No Point to plot
	}
	line->isShow = true;
	line->Color = Color;
	Coordinate_t *_pPoint = line->PointList;

	uint16_t _StartPoint = line->Head;//(line->isFull)? line->Tail : 0;
	uint16_t _StopPoint = (line->Tail)? line->Tail - 1 : PLOT_NUM_POINT_PER_LINE - 1;

	for(uint16_t i = _StartPoint; ; i++)
	{
		if(i >= PLOT_NUM_POINT_PER_LINE)
			i -= PLOT_NUM_POINT_PER_LINE;
		if(i == _StopPoint)
			break;
		uint16_t _NextPoint = i + 1;
		if(_NextPoint >= PLOT_NUM_POINT_PER_LINE)
			_NextPoint -= PLOT_NUM_POINT_PER_LINE;

		DrawLine(_pPoint[i], _pPoint[_NextPoint], Color);
	}

	return 0;

}

int Plot::Update()
{
	Clear();
	for(int i = 0; i<PLOT_NUM_LINE_PER_OBJ; i++)
	{
		if(LineList[i].isEnabled && LineList[i].isShow)
		{
			plotLine(&LineList[i], LineList[i].Color);
		}
	}
	return 0;
}
