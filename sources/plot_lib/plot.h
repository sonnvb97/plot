#ifndef PLOT_H
#define PLOT_H

#include "stm32l1xx.h"
#include <cstdlib>

#define PLOT_NUM_LINE_PER_OBJ	5
#define PLOT_NUM_POINT_PER_LINE			100

struct Coordinate_t
{
	int16_t x;
	int16_t y;
};

struct Position_t		// Pixel Position
{
	int16_t Column;
	int16_t Row;
};

struct Line_t
{
	bool	isEnabled;
	bool	isShow;
	bool	isFull;
	uint16_t Color;
	uint16_t Head;
	uint16_t Tail;
	Coordinate_t PointList[PLOT_NUM_POINT_PER_LINE];
};

class Plot
{
public:
	Plot(bool isTimeBased = false);
	virtual int Clear(){return 0;}
	int Update();

	Line_t* newLine();
	int		deleteLine(Line_t* &line);
	bool	lineExisted(Line_t* line);
	int		addPoint(Line_t* line, int16_t x, int16_t y);
	int		plotLine(Line_t* line, uint16_t Color);
	int		setScale(uint8_t scale);
	int		setScale(uint8_t scale_x, uint8_t scale_y);
	int		setAxisDrawScale(uint8_t scale);
	int		setAxisDrawScale(uint8_t scale_x, uint8_t scale_y);
	int		setDrawZone(int16_t width, int16_t height);
	int		drawAxis(uint16_t Color);
	int16_t getWidth();
	int16_t getHeight();
	
private:
	Position_t CoordinateToPosition(Coordinate_t Point);
	int		DrawPosition(int16_t Column, int16_t Row, uint16_t Color);
	int		DrawPoint(Coordinate_t Point);
	int		DrawLine(Coordinate_t Point1, Coordinate_t Point2, uint16_t Color);
	void	CalculateDisplay();
protected:
	bool	TimeBased;
	int16_t	xDispMax;
	int16_t	xDispMin;
	int16_t	yDispMax;
	int16_t	yDispMin;
	Line_t* FirstLine;
	Line_t* LastLine;
	uint8_t 	Scale_x;
	uint8_t		Scale_y;
	uint8_t		drawScale_x;
	uint8_t		drawScale_y;
	Position_t Origin;
	int16_t	DrawZone_Width;
	int16_t	DrawZone_Height;
	Line_t LineList[PLOT_NUM_LINE_PER_OBJ];
	uint8_t CurrentLineNumber;

	virtual int DrawPixel(int16_t Column, int16_t Row, uint16_t Color);
};

#endif // PLOT_H
