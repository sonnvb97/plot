CFLAGS		+= -I./sources/plot_lib
CPPFLAGS	+= -I./sources/plot_lib

VPATH += sources/plot_lib


# CPP source files
SOURCES_CPP += sources/plot_lib/plot.cpp
#SOURCES_CPP += sources/plot_lib/plot_ssd1306.cpp
SOURCES_CPP += sources/plot_lib/plot_monochrome.cpp
