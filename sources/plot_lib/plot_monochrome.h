#ifndef PLOT_SSD1306_H
#define PLOT_SSD1306_H

#include "plot.h"

class plot_monochrome: public Plot
{
private:
	int DrawPixel(int16_t Column, int16_t Row, uint16_t Color);
public:
	uint8_t Bitmap[1024];
	plot_monochrome(bool isTimeBased = false);
	int Clear();
};

#endif // PLOT_SSD1306_H
