#include "plot_monochrome.h"

plot_monochrome::plot_monochrome(bool isTimeBased):Plot(isTimeBased)
{
	for(int16_t i = 0; i < 1024; i++)
	{
		Bitmap[i] = 0;
	}
	drawAxis(1);
}

int plot_monochrome::DrawPixel(int16_t Column, int16_t Row, uint16_t Color)
{
	int16_t byteWidth = (DrawZone_Width+7)/8;// + (DrawZone_Width%8)? 1:0;
	int16_t currentBit = Column & 7;	//Column%8;
	int16_t currentByte = Row*byteWidth + Column/8;

	if(Color)
	{
		Bitmap[currentByte] |= 128>>currentBit;
	}
	else
	{
		Bitmap[currentByte] &= ~(128>>currentBit);
	}
	return 0;
}

int plot_monochrome::Clear()
{
	for(int16_t i = 0; i < 1024; i++)
	{
		Bitmap[i] = 0;
	}
	drawAxis(1);
	return 0;
}
