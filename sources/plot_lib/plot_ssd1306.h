#ifndef PLOT_SSD1306_H
#define PLOT_SSD1306_H

#include "plot.h"
#include "Adafruit_ssd1306syp.h"

class plot_ssd1306: public Plot
{
private:
	Adafruit_ssd1306syp Render;
	int DrawPixel(int16_t Column, int16_t Row, uint16_t Color);
	int Draw();
public:
	plot_ssd1306(bool isTimeBased = false);
	int initialize();
	int Update();
	int Clear();

};

#endif // PLOT_SSD1306_H
